<?php

/**
 * @file
 * Event type editing UI.
 */

/**
 * UI controller.
 */
class EventTypeUIController extends EntityDefaultUIController {

  /**
   * Overrides hook_menu() defaults.
   */
  public function hook_menu() {
    $items = parent::hook_menu();
		$items[$this->path]['title'] = 'Event types';
		$items[$this->path]['description'] = 'Manage event types, including adding
		and removing fields and the display of fields.';
    return $items;
  }
}

/**
 * Generates the event type editing form.
 */
function event_type_form($form, &$form_state, $event_type, $op = 'edit') {

  if ($op == 'clone') {
    $event_type->label .= ' (cloned)';
    $event_type->type = '';
  }

  $form['label'] = array(
    '#title' => t('Name'),
    '#type' => 'textfield',
    '#default_value' => $event_type->label,
    '#description' => t('The human-readable name of this Event type.'),
    '#required' => TRUE,
    '#size' => 30,
  );
  // Machine-readable type name.
  $form['type'] = array(
    '#type' => 'machine_name',
    '#default_value' => isset($event_type->type) ? $event_type->type : '',
    '#maxlength' => 32,
//    '#disabled' => $event_type->isLocked() && $op != 'clone',
    '#machine_name' => array(
      'exists' => 'event_get_types',
      'source' => array('label'),
    ),
    '#description' => t('A unique machine-readable name for this event type. It must only contain lowercase letters, numbers, and underscores.'),
  );

  $form['data']['#tree'] = TRUE;

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save event type'),
    '#weight' => 40,
  );

  //Locking not supported yet
  /*if (!$event_type->isLocked() && $op != 'add') {
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete event type'),
      '#weight' => 45,
      '#limit_validation_errors' => array(),
      '#submit' => array('event_type_form_submit_delete')
    );
  }*/
  return $form;
}

/**
 * Form API submit callback for the type form.
 */
function event_type_form_submit(&$form, &$form_state) {
  $event_type = entity_ui_form_submit_build_entity($form, $form_state);
  $event_type->save();
  $form_state['redirect'] = 'admin/structure/event_types';
}

/**
 * Form API submit callback for the delete button.
 */
function event_type_form_submit_delete(&$form, &$form_state) {
  $form_state['redirect'] = 'admin/structure/event_types/manage/' . $form_state['event_type']->type . '/delete';
}
