<?php

/**
 * @file
 * Event editing UI.
 *
 * We make very little use of the EntityAPI interface for this - preferring instead to use
 * views. That offers more flexibility to change a UI that will, more often than not,
 * be end-user facing.
 */

/**
 * UI controller.
 */
class EventUIController extends EntityDefaultUIController {

  /**
   * Overrides hook_menu() defaults. Main reason for doing this is that
   * parent class hook_menu() is optimized for entity type administration.
   */
  public function hook_menu() {
    
    $items = array();
    $id_count = count(explode('/', $this->path));
    $wildcard = isset($this->entityInfo['admin ui']['menu wildcard']) ? $this->entityInfo['admin ui']['menu wildcard'] : '%' . $this->entityType;

    $items[$this->path] = array(
      'title' => 'Events',
      'description' => 'Add edit and update events.',
      'page callback' => 'system_admin_menu_block_page',
      'access arguments' => array('access administration pages'),
      'file path' => drupal_get_path('module', 'system'),
      'file' => 'system.admin.inc',
    );
    
    // Change the overview menu type for the list of events.
    $items[$this->path]['type'] = MENU_LOCAL_TASK;
    
    // Change the add page menu to multiple types of entities
    $items[$this->path . '/add'] = array(
      'title' => 'Add an event',
      'description' => 'Add a new event',
      'page callback'  => 'event_add_page',
      'access callback'  => 'event_access',
      'access arguments' => array('edit'),
      'type' => MENU_NORMAL_ITEM,
      'weight' => 20,
      'file path' => drupal_get_path('module', $this->entityInfo['module']).'/includes',
      'file' => 'event.admin.inc',
    );
    
    // Add menu items to add each different type of entity.
    foreach (event_get_types() as $type) {
      $items[$this->path . '/add/' . $type->type] = array(
        'title' => 'Add ' . $type->label,
        'page callback' => 'event_form_wrapper',
        'page arguments' => array(event_create(array('type' => $type->type))),
        'access callback' => 'event_access',
        'access arguments' => array('edit', 'edit ' . $type->type),
        'file path' => drupal_get_path('module', $this->entityInfo['module']).'/includes',
        'file' => 'event.admin.inc',
      );
    }
    
    // Loading and editing event entities
    $items[$this->path . '/event/' . $wildcard] = array(
      'page callback' => 'event_form_wrapper',
      'page arguments' => array($id_count + 1),
      'access callback' => 'event_access',
      'access arguments' => array('edit', $id_count + 1),
      'weight' => 0,
      'context' => MENU_CONTEXT_PAGE | MENU_CONTEXT_INLINE,
      'file path' => drupal_get_path('module', $this->entityInfo['module']).'/includes',
      'file' => 'event.admin.inc',
    );
    
    $items['event/%event/view'] = array(
      'title' => 'View',
      'page callback' => 'event_page_view',
      'page arguments' => array(1),
      'access callback' => 'event_access',
      'access arguments' => array('view', 1),
      'weight' => -6,
      'type' => MENU_DEFAULT_LOCAL_TASK,
    );

    $items['event/%event/edit'] = array(
      'title' => 'Edit',
      'page callback' => 'event_form_wrapper',
      'page arguments' => array(1),
      'access callback' => 'event_access',
      'access arguments' => array('administer events'),
      'type' => MENU_LOCAL_TASK,
      'weight' => -5,
      'file path' => drupal_get_path('module', $this->entityInfo['module']).'/includes',
      'file' => 'event.admin.inc',
    );
    
    $items['event/%event/delete'] = array(
      'title' => 'Delete',
      'page callback' => 'event_delete_form_wrapper',
      'page arguments' => array(1),
      'access callback' => 'event_access',
      'access arguments' => array('edit', $id_count + 1),
      'type' => MENU_LOCAL_TASK,
      'context' => MENU_CONTEXT_INLINE,
      'weight' => 10,
      'file path' => drupal_get_path('module', $this->entityInfo['module']).'/includes',
      'file' => 'event.admin.inc',
    );
    
    // Menu item for viewing events
    $items['event/%event'] = array(
      //'title' => 'Title',
      'title callback' => 'event_page_title',
      'title arguments' => array(1),
      'page callback' => 'event_page_view',
      'page arguments' => array(1),
      'access callback' => 'event_access',
      'access arguments' => array('view', 1),
      'type' => MENU_CALLBACK,
    );
    
    if (module_exists('devel')) {
      $items['event/%event/devel'] = array(
        'title' => 'Devel',
        'page callback' => 'devel_load_object',
        'page arguments' => array('node', 1),
        'access arguments' => array('access devel information'),
        'type' => MENU_LOCAL_TASK,
        'file path' => drupal_get_path('module', 'devel'),
        'file' => 'devel.pages.inc',
        'weight' => 100,
      );
      
      $items['event/%event/devel/load'] = array(
        'title' => 'Load',
        'type' => MENU_DEFAULT_LOCAL_TASK,
      );
    }
    
    return $items;
  }
  
  
  /**
   * Create the markup for the add Event Entities page within the class
   * so it can easily be extended/overriden.
   */ 
  public function addPage() {
    $item = menu_get_item();
    $content = system_admin_menu_block($item);

    if (count($content) == 1) {
      $item = array_shift($content);
      drupal_goto($item['href']);
    }    
        
    return theme('event_add_list', array('content' => $content));
  }
  
}


/**
 * Form callback wrapper: create or edit an event.
 *
 * @param $event
 *   The event object being edited by this form.
 *
 * @see event_edit_form()
 */
function event_form_wrapper($event) {
  // Add the breadcrumb for the form's location.
  event_set_breadcrumb();
  return drupal_get_form('event_edit_form', $event);
}


/**
 * Form callback wrapper: delete an event.
 *
 * @param $event
 *   The event object being edited by this form.
 *
 * @see event_edit_form()
 */
function event_delete_form_wrapper($event) {
  // Add the breadcrumb for the form's location.
  //event_set_breadcrumb();
  return drupal_get_form('event_delete_form', $event);
}


/**
 * Form callback: create or edit an event.
 *
 * @param $event
 *   The event object to edit or for a create form an empty event object
 *     with only an event type defined.
 */
function event_edit_form($form, &$form_state, $event) {
  if(!isset($event->is_new)) {
    drupal_set_title('Editing '.$event->title);
  }
  // Add the default field elements.
  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Event Title'),
    '#default_value' => isset($event->title) ? $event->title : '',
    '#maxlength' => 255,
    '#required' => TRUE,
    '#weight' => -5,
  );
  
  $form['data']['#tree'] = TRUE;

  // Add the field related form elements.
  $form_state['event'] = $event;
  field_attach_form('event', $event, $form, $form_state);

  $form['actions'] = array(
    '#type' => 'container',
    '#attributes' => array('class' => array('form-actions')),
    '#weight' => 400,
  );
  
  $form['options']['status'] = array(
    '#type' => 'checkbox',
    '#title' => t('Published'),
    '#default_value' => isset($event->is_new) ? '1' : $event->status,
  );

  // We add the form's #submit array to this button along with the actual submit
  // handler to preserve any submit handlers added by a form callback_wrapper.
  $submit = array();

  if (!empty($form['#submit'])) {
    $submit += $form['#submit'];
  }

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save event'),
    '#submit' => $submit + array('event_edit_form_submit'),
  );
  
  if (!empty($event->title)) {
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete event'),
      '#suffix' => l(t('Cancel'), 'admin/content/event'),
      '#submit' => $submit + array('event_form_submit_delete'),
      '#weight' => 45,
    );
  }

  // We append the validate handler to #validate in case a form callback_wrapper
  // is used to add validate handlers earlier.
  $form['#validate'][] = 'event_edit_form_validate';
  return $form;
}


/**
 * Form API validate callback for the event form
 */
function event_edit_form_validate(&$form, &$form_state) {
  $event = $form_state['event'];
  
  // Notify field widgets to validate their data.
  field_attach_form_validate('event', $event, $form, $form_state);
}


/**
 * Form API submit callback for the event form.
 * 
 * @todo remove hard-coded link
 */
function event_edit_form_submit(&$form, &$form_state) {
  
  $event = entity_ui_controller('event')->entityFormSubmitBuildEntity($form, $form_state);
  // Save the event and go back to the list of events
  
  // Add in created and changed times.
  if ($event->is_new = isset($event->is_new) ? $event->is_new : 0){
    $event->created = time();
  }

  $event->changed = time();
  
  $event->save();
  $form_state['redirect'] = 'admin/content/event';
}

/**
 * Form API submit callback for the delete button.
 * 
 * @todo Remove hard-coded path
 */
function event_form_submit_delete(&$form, &$form_state) {
  $form_state['redirect'] = 'event/' . $form_state['event']->event_id . '/delete';
}


/**
 * Form callback: confirmation form for deleting an event.
 *
 * @param $event
 *   The event to delete
 *
 * @see confirm_form()
 */
function event_delete_form($form, &$form_state, $event) {
  $form_state['event'] = $event;

  $form['#submit'][] = 'event_delete_form_submit';

  $form = confirm_form($form,
    t('Are you sure you want to delete this event?'),
    'admin/content/event',
    '<p>' . t('This action cannot be undone.') . '</p>',
    t('Delete'),
    t('Cancel'),
    'confirm'
  );
  
  return $form;
}

/**
 * Submit callback for event_delete_form
 */
function event_delete_form_submit($form, &$form_state) {
  $event = $form_state['event'];

  event_delete($event);

  drupal_set_message(t('The event %title has been deleted.', array('%title' => $event->title)));
  watchdog('event', 'Deleted event %title.', array('%title' => $event->title));

  $form_state['redirect'] = 'admin/content/event';
}



/**
 * Page to add Event Entities.
 *
 * @todo Pass this through a proper theme function
 */
function event_add_page() {
  $controller = entity_ui_controller('event');
  return $controller->addPage();
}


/**
 * Displays the list of available event types for event creation.
 *
 * @ingroup themeable
 */
function theme_event_add_list($variables) {
  $content = $variables['content'];
  $output = '';
  if ($content) {
    $output = '<dl class="event-type-list">';
    foreach ($content as $item) {
      $output .= '<dt>' . l($item['title'], $item['href']) . '</dt>';
      $output .= '<dd>' . filter_xss_admin($item['description']) . '</dd>';
    }
    $output .= '</dl>';
  }
  else {
    if (user_access('administer event types')) {
      $output = '<p>' . t('Events cannot be added because you have not created any event types yet. Go to the <a href="@create-event-type">event type creation page</a> to add a new event type.', array('@create-event-type' => url('admin/structure/event_types/add'))) . '</p>';
    }
    else {
      $output = '<p>' . t('No event types have been created yet for you to use.') . '</p>';
    }
  }

  return $output;
}


/**
 * Sets the breadcrumb for administrative event pages.
 */
function event_set_breadcrumb() {
  $breadcrumb = array(
    l(t('Home'), '<front>'),
    l(t('Administration'), 'admin'),
    l(t('Content'), 'admin/content'),
    l(t('Events'), 'admin/content/event'),
  );

  drupal_set_breadcrumb($breadcrumb);
}



