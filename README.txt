Event module
-----------------
by Sean Jorgensen, jorgensean on drupal.org

--------------------------------------------------------------------------------
                                Ev((ent)ity) Module
--------------------------------------------------------------------------------

Based on the excellent entity kickstarter Model, this is a completed, packaged
event as an entity. If you need this, you probably are looking for it. If for
some reason, a Node Content Type event won't work for you, then this module is
for you.

It comes with views integration, devel integration, and of course, field integration.
There is no date field included with an event by default, I leave it up to the site
builder to decide which date format/field type they want to store their date in.

A default view is installed with this module, adding a tab to the content menu for
events. It truly becomes useful if you customize it with the date field you create
for your event type(s). It also demonstrates views integration/ease.

Event types can be added/configured/fielded at admin/structure/event_types
Event administration at admin/content/event